RS_ACCOUNT = "201143875aed70001ef8a2b986a89f1e"
RS_PASSWORD = "prueba"
# Retroshare endpoints
RS_URL = "http://127.0.0.1:9092/"

# XRCB.cat endpoints
XRCB_RADIOS_ALL = "https://xrcb.cat/ca/radios-json/"
XRCB_RADIO_PODCASTS = "https://xrcb.cat/ca/podcasts-json/?radio_id="

# Used to store the radio/podcast id on the description to easly check relationships between
# radio <-> channel podcast <-> post
XRCB_TOKENID_KEY = "internalId"
XRCB_TOKENID = "{{\""+XRCB_TOKENID_KEY+"\":{id}}}"
# RS channel name
XRCB_CHANNEL_NAME = "XRCB.cat - {title}"
# RS channel description
XRCB_CHANNEL_DESC = "{title}\nXarxa Radios Comunitaries Barcelona \nRadio del barri del {barrio}\n{permalink}\n{web}\n"+XRCB_TOKENID
# RS post title
XRCB_POST_TITLE = "{title}"
# RS post description
XRCB_POST_DESCRIPTION = "{description}\nWebFile: {file_mp3}\nPublication: {permalink}\nRadio: {radio_permalink}\n"+XRCB_TOKENID


# File Sharing
SHARED_DIR = "/tmp"
SHARED_VIRTUALNAME = "xrcb.cat"

DEBUG = True