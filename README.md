# Installation

Install `requirments.txt` using:

```bash
pip install -r requirments.txt 
```

Also I installed some packages for use [regex library from Pyhont](https://pypi.org/project/regex/). This library is 
installed because support recursively search inside strings, used to look fordward for find a json inside description of
channels and posts. Inside this Json is stored the relationship between the channels<->radios and posts<->podcasts:

```bash
sudo apt-get install python3 python3-pip python-dev python3-dev \
     build-essential libssl-dev libffi-dev \
     libxml2-dev libxslt1-dev zlib1g-dev
```

# Usage

```
Welcome to XRCB.cat to Retroshare API

Available options:
	--radios -r Update channels from radios
	--podcasts -p Update posts from podcasts
	--unsubscribe -u unsubscrive all channels
	--share -s share the directory defined on config
	--help -h print this help
```

Tipical workflow:

1. `python run.py -r` This will create/update channel for each radio
2. `python run.py -s` This share the mp3 directory to make it available from RS share files system
3. `python run.py -p` This create/update a post for each podcasts of each radio
