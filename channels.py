

import config
import requests, json, os, hashlib


RS_CHANS_GET = "rsGxsChannels/getChannelsSummaries"
RS_CHANS_GET_INFO = "rsGxsChannels/getChannelsInfo"
RS_CHAN_CREATE = "rsGxsChannels/createChannel"
RS_CHAN_SUSCRIBE = "rsGxsChannels/subscribeToChannel"
# Posts
RS_POSTS_GET = "rsGxsChannels/getContentSummaries"
RS_POST_GET = "rsGxsChannels/getChannelContent"
RS_POST_CREATE = "rsGxsChannels/createPost"
# File Sharing
RS_SHARE_DIR = "rsFiles/addSharedDirectory"

jsonApiUrl = config.RS_URL
basicAuthCredentials = (config.RS_ACCOUNT, config.RS_PASSWORD)


def debugDump(label, data):
    if not config.DEBUG: return
    print(label, json.dumps(data, sort_keys=True, indent=4))

def jsonApiCall(function, data = None):
    url = jsonApiUrl + function

    debugDump('POST: ' + url, data)
    resp = requests.post(url=url, json=data, auth=basicAuthCredentials)

    debugDump('RESP', resp.json())
    return resp.json()

def createChannel(title, description):
    createChannelJson = {"channel":
                             {"mMeta":
                                  {"mGroupName": title,
                                   "mGroupFlags": 4,
                                   "mSignFlags": 520},
                              "mDescription": description},
                         "caller_data": ""}
    jsonApiCall(RS_CHAN_CREATE, createChannelJson)

def getChannels():
    return jsonApiCall(RS_CHANS_GET)

def getChannelsInfo(channelsIdArray):
    json = { "chanIds" : channelsIdArray }
    return jsonApiCall(RS_CHANS_GET_INFO, json)["channelsInfo"]


def subscribe(channelId, subscribe=True):
    json = { "channelId" : channelId , "subscribe" : subscribe}
    return jsonApiCall(RS_CHAN_SUSCRIBE, json)


def unsuscribe(channelId):
    return subscribe(channelId, subscribe=False)

def unsubscribeAll():
    chans = getChannels()
    for chan in chans["channels"]:
        if (chan["mSubscribeFlags"] == 7):
            unsuscribe(chan["mGroupId"])

def getSubscribedChannels():
    chans = getChannels()["channels"]
    res = []
    for chan in chans:
        if (chan["mSubscribeFlags"] == 7):
            res.append(chan)
    return res

# Get details of subscribed channels calling to getChannelContent
def getSubscribedChannelsInfo():
    chans = getSubscribedChannels()
    idsArray = []
    for chan in chans:
        idsArray.append(chan["mGroupId"])
    return getChannelsInfo(idsArray)



def getPostSummaries(channelId):
    json = { "channelId" : channelId }
    return jsonApiCall(RS_POSTS_GET, json)

def getPostSummariesInfo(channelId):
    posts = getPostSummaries(channelId)
    postList = []
    for post in posts["summaries"]:
        postList.append(post["mMsgId"])

    json = {"channelId" : channelId, "contentsIds": postList}
    return jsonApiCall(RS_POST_GET, json)

def createPost(chanId, title, description, file=None):
    createChannelJson = {"post" : {
                            "mMeta": {
                                "mGroupId" : chanId,
                                "mMsgFlags" : 0,
                                "mMsgName" : title
                            },
                        "mFiles" : [],
                        "mMsg" : description

                        }, "caller_data":""
                    }
    if file is not None:
        fh = open(file, 'r')
        fByteSize = getFileBytes(file)
        sha1sum = getSha1Sum(file)
        mFileJson = {
            "mName": file,
            "mHash": str(sha1sum),
            "mSize": fByteSize
        }
        createChannelJson["post"]["mFiles"].append(mFileJson)

    jsonApiCall(RS_POST_CREATE, createChannelJson)


def getFileBytes(f):
    return os.path.getsize(f)

# From https://stackoverflow.com/questions/22058048/hashing-a-file-in-python
def getSha1Sum(f):
    # BUF_SIZE is totally arbitrary, change for your app!
    BUF_SIZE = 65536  # lets read stuff in 64kb chunks!
    sha1 = hashlib.sha1()
    with open(f, 'rb') as file:
        while True:
            data = file.read(BUF_SIZE)
            if not data:
                break
            sha1.update(data)
    return sha1.hexdigest()

def addSharedDir(sharedDir, vName):
    json= {
        "dir" : {
            "filename": sharedDir,
            "virtualname": vName,
            "shareflags": 128},
        "caller_data": ""}
    jsonApiCall(RS_SHARE_DIR, json)

