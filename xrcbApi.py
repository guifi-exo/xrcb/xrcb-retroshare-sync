'''
Script used for the creation of RS channels based on radio level for XRCB.cat
1. It takes all radios from config.XRCB_RADIOS_ALL
2. Create a channel for each radio called
'''
import config
import urllib.request, json, regex
from channels import debugDump
import channels

urlRadiosAll = config.XRCB_RADIOS_ALL
urlRadioPodcasts = config.XRCB_RADIO_PODCASTS
chanName = config.XRCB_CHANNEL_NAME
chanDesc = config.XRCB_CHANNEL_DESC
postTitle = config.XRCB_POST_TITLE
postDesc = config.XRCB_POST_DESCRIPTION

def parseJsonUrl(url):
    try:
        with urllib.request.urlopen(url) as url:
            data = json.loads(url.read().decode())
            return data
    except:
        return None


# Return XRCB channel name formated
def formatChanName(s=chanName, radio=None):
    return s.format(title=radio["title"])
# Return channel description formated
def formatChanDescription(s=chanDesc, radio=None):
    return s.format(title=radio["title"],
                    barrio=radio["barrio"],
                    permalink=radio["permalink"],
                    web=radio["web"],
                    id=radio["id"])

def formatPostTitle(podcast):
    return postTitle.format(title=podcast["title"])

def formatPostDescription(podcast):
    return postDesc.format(description=podcast["description"],
                           file_mp3=podcast["file_mp3"],
                           permalink=podcast["permalink"],
                           radio_permalink=podcast["radio_permalink"],
                           id=podcast["id"])

# Get all radios from wordpress api
def getAllRadios():
    debugDump('GET: ' + urlRadiosAll, "Get all radios")
    return parseJsonUrl(urlRadiosAll)

def cmprRadioWithChannel(radio, channel):
    return radio["id"] == getIdFromDescription(channel["mDescription"])

def checkIfRadioExist(radio, chans):
    for chan in chans:
        # Get if has a radio id
        radioId = getIdFromDescription(chan["mDescription"])
        if radioId is not None:
            if cmprRadioWithChannel(radio, chan):
                return True
    return False

# Api call to get all podcast for radio based on radio id
def getPodcastPerRadio(radioId):
    url = urlRadioPodcasts+radioId
    debugDump('GET: ' + url, "Get podcast for radio " + radioId)
    return parseJsonUrl(url)

# get the radio id from the json on the description of the channel on RS
# looking for config.XRCB_TOKENID
def getIdFromDescription(rsDescritpion):
    pattern = regex.compile(r'\{(?:[^{}]|(?R))*\}')
    try:
        return json.loads(pattern.findall(rsDescritpion)[0])[config.XRCB_TOKENID_KEY]
    except:
        return None

# Compare a podcasts id with post id on description
def cmprPodcastWithPosts(podcast, post):
    return podcast["id"] == getIdFromDescription(post["mMsg"])


# Check if podcast is publshed on a postSummaries
def isPodcastPublished(podcast, postSummaries):
    for post in postSummaries:
        if cmprPodcastWithPosts(podcast, post):
            return True
    return False

# Compare a podcast list with summaries list, if podcast is not published, publish it!
def updatePosts(podcasts, posts, chanId):
    for podcast in podcasts:
        if isPodcastPublished(podcast, posts):
            debugDump("PODCAST-UP-TO-DATE: ", podcast)
            print("Podcast is up to date ", podcast)
        else:
            debugDump("NEW-PODCAST", podcast)
            print("Podcast NOT published!",podcast)
            file = None
            if podcast["file_mp3"]:
                file = podcast["file_mp3"].replace('https://xrcb.cat/wp-content', config.SHARED_DIR)
            channels.createPost(chanId, formatPostTitle(podcast), formatPostDescription(podcast), file)

def updateChannelsFromRadios():
    radiosJson = getAllRadios()

    chans = channels.getSubscribedChannelsInfo()
    for radio in radiosJson["data"]:
        exists =  checkIfRadioExist(radio, chans)
        print("Radio", radio["title"], " exists: ",exists)
        if not exists:
            channels.createChannel(formatChanName(radio=radio), formatChanDescription(radio=radio))

def updatePostsFromPodcasts():
    # Get all subscribed channels
    chans = channels.getSubscribedChannelsInfo()
    for chan in chans:
        # Get if has a radio id
        radioId = getIdFromDescription(chan["mDescription"])
        if radioId is not None:
            # Get summaries and compare that all posts are published
            chanId = chan["mMeta"]["mGroupId"]
            sums = channels.getPostSummariesInfo(chanId)
            # If radio has podcasts check
            podcasts = getPodcastPerRadio(str(radioId))
            if podcasts is not None:
                # check if a podcast is published
                updatePosts(podcasts["data"], sums["posts"], chanId)

def unsubscribe():
    channels.unsubscribeAll()

def addShareDirectory():
    sharedDir = config.SHARED_DIR
    vName = config.SHARED_VIRTUALNAME
    channels.addSharedDir(sharedDir, vName)
