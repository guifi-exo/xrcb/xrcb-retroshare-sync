import sys, getopt
import xrcbApi

def updateChannelsFromRadios():
    print("xrcbApi.updateChannelsFromRadios()")
    xrcbApi.updateChannelsFromRadios()

def updatePostsFromPodcasts():
    print("xrcbApi.updatePostsFromPodcasts()")
    xrcbApi.updatePostsFromPodcasts()

def unsubscribe():
    print("xrcbApi.unsubscribe()")
    xrcbApi.unsubscribe()

def addShareDirectory():
    print("xrcbApi.addShareDirectory()")
    xrcbApi.addShareDirectory()


# Main
def main(argv):

    def printHelp():
        print("Welcome to XRCB.cat to Retroshare API")
        print("\nAvailable options:")
        print("\t--radios -r Update channels from radios")
        print("\t--podcasts -p Update posts from podcasts")
        print("\t--unsubscribe -u unsubscrive all channels")
        print("\t--share -s share the directory defined on config")
        print("\t--help -h print this help")

    if argv.__len__() == 0:
        printHelp()
    try:
        opts, args = getopt.getopt(argv, "hrpus",
                                   ["help", "radios", "podcasts", "unsubscribe", "share"])  # Letters recognized. If a letter has argument follow it with a colon :, like p: fill be '-p arg'
    except getopt.GetoptError:
        printHelp()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            printHelp()
            break
        elif opt in ("-r", "--radios"):
            updateChannelsFromRadios()
        elif opt in ("-p", "--podcasts"):
            updatePostsFromPodcasts()
        elif opt in ("-u", "--unsubscribe"):
            unsubscribe()
        elif opt in ("-s", "--share"):
            addShareDirectory()

if __name__ == '__main__':
    main(sys.argv[1:])
